package demo.question;

import org.junit.Test;
import org.osgl.inject.Genie;
import org.osgl.inject.annotation.Configuration;
import org.osgl.inject.loader.ConfigurationValueLoader;
import org.osgl.util.IO;
import osgl.ut.TestBase;

import java.util.Properties;

public class GenieTester2 extends TestBase {

	public static class ConfigurationLoader extends ConfigurationValueLoader {

		private Properties prop;

		@Override
		protected void initialized() {
			super.initialized();
			prop = IO.loadProperties(GenieTester2.class.getResource("/test.properties"));
		}

		@Override
		protected Object conf(String key, String defaultValue) {
			return prop.getProperty(key, defaultValue);
		}
	}

	public static abstract class Car {
		@Configuration("color")
		public String color;
		@Configuration("seats")
		public int seats;
	}

	public static class Car1 extends Car {
	}

	public static class Car2 extends Car {
	}

	public static class TestModule extends org.osgl.inject.Module {
		@Override
		protected void configure() {
			bind(ConfigurationValueLoader.class).to(ConfigurationLoader.class);
			bind(Car.class).to(Car1.class);
		}
	}


	@Test
	public void text() {
		Genie genie = Genie.create(new TestModule());
		Car car = genie.get(Car.class);
		yes(car instanceof Car1);
		eq("red", car.color);
		eq(6, car.seats);
	}
}