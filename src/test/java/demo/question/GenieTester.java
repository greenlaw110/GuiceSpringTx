package demo.question;

import org.junit.Test;
import org.osgl.inject.Genie;
import org.osgl.inject.Module;
import osgl.ut.TestBase;

import javax.inject.Named;

public class GenieTester extends TestBase {


	public static abstract class Car {
		public String color;
	}

	public static class Car1 extends Car {
		public Car1(@Named("color") String color) {
			this.color = color;
		}
	}

	public static class Car2 extends Car {
		public Car2(@Named("color") String color) {
			this.color = color;
		}
	}

	public static class TestModule extends Module {
		@Override
		protected void configure() {
			bind(String.class).named("color").to("red");
			bind(Car.class).toConstructor(Car1.class, String.class);
		}
	}

	@Test
	public void text() {
		Genie genie = Genie.create(new TestModule());
		Car car = genie.get(Car.class);
		yes(car instanceof Car1);
		eq("red", car.color);
	}
}